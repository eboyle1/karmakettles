<?php

$url = 'https://io.adafruit.com/api/v2/telstar505/feeds';

$file = './Data/data_php1.csv';

$timestamp = time();
$timestring = date("d-M-y-H-i-s", $timestamp);
$cur_sec = date("s", $timestamp);

$JSON = file_get_contents($url);

$json_array = json_decode($JSON, true);

foreach($json_array as $item)
{
	$name = $item['name'];
	$value = $item['last_value'];

	if ($name == "grid")
	{
		$cur_grid_val = $value;
	}
	elseif ($name == "LocalStorage")
	{
		$cur_ls_val = $value;
	}
	elseif ($name == "karmak1")
	{
		$cur_k1_val = $value;
	}
	elseif ($name == "karmak2")
	{
		$cur_k2_val = $value;
	}
	elseif ($name == "stork1")
	{
		$cur_stork1_val = $value;
	}
	elseif ($name == "stork2")
	{
		$cur_stork2_val = $value;
	}
	elseif ($name == "Boil")
	{
		$cur_boil_val = $value;
	}
	elseif ($name == "Pull")
	{
		$cur_pull_val = $value;
	}
	elseif ($name == "Push")
	{
		$cur_push_val = $value;
	}
	elseif ($name == "GridText")
	{
		$cur_gtext_val = $value;
	}
	elseif ($name == "StorageText")
	{
		$cur_stext_val = $value;
	}
	elseif ($name == "Time")
	{
		$cur_ftime_val = $value;
	}
}

$datastring = $timestring . "," . $cur_grid_val . "," . $cur_ls_val . "," . $cur_k1_val . "," . $cur_k2_val . "," . $cur_stork1_val . "," . $cur_stork2_val . "," . $cur_boil_val . "," . $cur_pull_val . "," . $cur_push_val . "," . $cur_gtext_val . "," . $cur_stext_val . "," . $cur_ftime_val . ":" . $cur_sec . "\n";

$file_out = file($file);
unset($file_out[1]);
file_put_contents($file, implode("", $file_out));

file_put_contents ($file, $datastring, FILE_APPEND);

?>